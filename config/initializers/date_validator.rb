class DateValidator < ActiveModel::Validator
  def validate(date)
    value=date.date
    if value < Time.zone.now
      date.errors[:date] << 'cannot be past current time.'
    end
    if value.hour < 8
      date.errors[:date] << 'cannot be before 8:00 AM.'
    end
    if value.hour > 16
      date.errors[:date] << 'cannot be after 4:30 PM.'
    end
    if value > Time.zone.now+180.days
      date.errors[:base] << 'cannot be greater than 180 days from current time.'
    end
  end
end