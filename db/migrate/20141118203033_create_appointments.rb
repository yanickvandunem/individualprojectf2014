class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.datetime :date
      t.string :patientConcern
      t.string :physicianNotes
      t.integer :patientId
      t.integer :physicianId
      t.integer :diagnosticCodeId

      t.timestamps
    end
  end
end
