class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :patientId
      t.integer :appointmentId
      t.decimal :subtotal

      t.timestamps
    end
  end
end
