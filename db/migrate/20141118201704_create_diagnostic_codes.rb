class CreateDiagnosticCodes < ActiveRecord::Migration
  def change
    create_table :diagnostic_codes do |t|
      t.string :code
      t.string :description
      t.decimal :price

      t.timestamps
    end
  end
end
