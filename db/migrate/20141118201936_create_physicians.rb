class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :name
      t.string :email
      t.string :speciality

      t.timestamps
    end
  end
end
