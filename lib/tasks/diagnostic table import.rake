namespace :import_diagnostic do
  desc 'import the diagnostic codes from csv'
  task :data => :environment do
    require 'csv'
    CSV.foreach('\bin\diagnostic codes.csv') do |row|
    code = row[0]
    description = row[1]
    price = row[2]
    DiagnosticCode.create(:code => code, :description => description, :price => price)
    end
end
end