/**
 * Created by Yanick on 11/20/2014.
 */


//document.getElementById("appointment").disabled = true;
$(function() {
    $( "#appointment" ).datepicker({
        dateFormat: "DD, d MM, yy",
        showOn: "button",
        buttonImage: "//jqueryui.com/resources/demos/datepicker/images/calendar.gif",
        buttonImageOnly: true,
        altField: "#appointment_date_hidden",
        minDate: 0,
        maxDate: 90,
        beforeShowDay: $.datepicker.noWeekends,
        altFormat: "yy-mm-dd"
    });
    });


$(document).on('change','#appointment_physicianId', function gethours(e){
    var selectedDay, selectedPhysician;
    selectedPhysician = $("#appointment_physicianId").val();
    selectedDay = $("#appointment_date_hidden").val();
    if (selectedPhysician !== '' && selectedPhysician !== undefined && selectedDay !== '' && selectedDay !== undefined) {
        $("#appointment_date").empty();
        $("#appointment_date").prepend("<option value=''>Loading</option>").val('');
        $("#appointment_date").load("/appointment-hours?date="+selectedDay+"&physicianId="+selectedPhysician+"&currentAppointment= #appointment_date > option")
    }

});

$(document).on('change','#appointment', function gethours(e){
    var selectedDay, selectedPhysician;
    selectedPhysician = $("#appointment_physicianId").val();
    selectedDay = $("#appointment_date_hidden").val();
    if (selectedPhysician !== '' && selectedPhysician !== undefined && selectedDay !== '' && selectedDay !== undefined) {
        $("#appointment_date").empty();
        $("#appointment_date").prepend("<option value=''>Loading</option>").val('');
        $("#appointment_date").load("/appointment-hours?date="+selectedDay+"&physicianId="+selectedPhysician+"&currentAppointment= #appointment_date > option")
    }
});