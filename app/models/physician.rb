class Physician < ActiveRecord::Base
  has_many :appointments, :foreign_key => :physicianId
  has_many :patients, through: :appointments

  validates :name, presence: true
  validates :email, presence: true
end
