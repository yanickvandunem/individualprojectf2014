class Appointment < ActiveRecord::Base
  belongs_to :patients
  belongs_to :physicians
  has_one :invoice
  validates_uniqueness_of :date, scope: :physicianId
  validates :patientId, :physicianId, presence: true
  validates_with DateValidator
end
