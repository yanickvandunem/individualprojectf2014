class DiagnosticCode < ActiveRecord::Base
  validates :code, presence: true, uniqueness: true
  validates :description, :price, presence: true
end
