class Patient < ActiveRecord::Base
  has_many :appointments, :foreign_key => :patientId
  has_many :physicians, :through => :appointments


  validates :name, presence: true
  validates :email, presence: true
end
