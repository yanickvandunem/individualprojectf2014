class Invoice < ActiveRecord::Base
  belongs_to :appointment

  validates :appointmentId, :presence => true, uniqueness: true
end
