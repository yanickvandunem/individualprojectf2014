json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :date, :patientConcern, :physicianNotes, :patientId, :physicianId, :diagnosticCodeId
  json.url appointment_url(appointment, format: :json)
end
