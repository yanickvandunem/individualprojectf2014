json.array!(@invoices) do |invoice|
  json.extract! invoice, :id, :patientId, :appointmentId, :subtotal
  json.url invoice_url(invoice, format: :json)
end
