class WelcomesController < ApplicationController
  before_action :set_welcome, only: [:show, :edit, :update, :destroy]

  # GET /welcomes
  # GET /welcomes.json
  def index
    @welcomes = Welcome.all
  end

  # GET /welcomes/1
  # GET /welcomes/1.json
  def show
  end

  def about
  end

  def news
  end

  def administrator
    @physicians = Physician.all
    @physician = Physician.new
    @appointments = Appointment.all
    @diagnostic_code = DiagnosticCode.new
    @diagnostic_codes = DiagnosticCode.all
    @invoices = Invoice.all
  end

  def officeworker
    @invoice = Invoice.new
    @invoices = Invoice.all
    query = Appointment.where("date <'"+Time.zone.now.to_s+"'") #find all past appointments
    invoiced_appointment_ids = ''
    imax = query.count
    i =0
    query.each do |appointment|
      if !Invoice.find_by_appointmentId(appointment.id).nil? # only select the appointments that have not been invoiced
        invoiced_appointment_ids += 'id != '+ appointment.id.to_s
        if i < imax
          invoiced_appointment_ids += ' and '
          i +=1
        end
      else
        imax-=1
      end
    end
    if invoiced_appointment_ids.last(5) == ' and ';invoiced_appointment_ids=invoiced_appointment_ids[0..-5]  end
    @invoices_pending = query.where(invoiced_appointment_ids) #complete query
  end



  # POST /welcomes
  # POST /welcomes.json
  def create
    @welcome = Welcome.new(welcome_params)

    respond_to do |format|
      if @welcome.save
        format.html { redirect_to @welcome, notice: 'Welcome was successfully created.' }
        format.json { render :show, status: :created, location: @welcome }
      else
        format.html { render :new }
        format.json { render json: @welcome.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /welcomes/1
  # PATCH/PUT /welcomes/1.json
  def update
    respond_to do |format|
      if @welcome.update(welcome_params)
        format.html { redirect_to @welcome, notice: 'Welcome was successfully updated.' }
        format.json { render :show, status: :ok, location: @welcome }
      else
        format.html { render :edit }
        format.json { render json: @welcome.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /welcomes/1
  # DELETE /welcomes/1.json
  def destroy
    @welcome.destroy
    respond_to do |format|
      format.html { redirect_to welcomes_url, notice: 'Welcome was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_welcome
      @welcome = Welcome.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def welcome_params
      params.require(:welcome).permit(:index)
    end
end
