class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]

  # GET /appointments
  # GET /appointments.json
  def index
    @appointments = Appointment.all
  end

  # GET /appointments/1
  # GET /appointments/1.json
  def show
  end

  # GET /appointments/new
  def new
    @appointment = Appointment.new
  end

  # GET /appointments/1/edit
  def edit
  end

  # POST /appointments
  # POST /appointments.json
  def create

    @appointment = Appointment.new(appointment_params)


    respond_to do |format|

        if @appointment.save
        format.html { redirect_to @appointment, notice: 'Appointment was successfully created.' }
        format.json { render :show, status: :created, location: @appointment }
      else
        format.html { render :new}
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
      end
    end


  # PATCH/PUT /appointments/1
  # PATCH/PUT /appointments/1.json
  def update
    respond_to do |format|
      if @appointment.update(appointment_params)
        format.html { redirect_to @appointment, notice: 'Appointment was successfully updated.' }
        format.json { render :show, status: :ok, location: @appointment }
      else
        format.html { render :edit }
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appointments/1
  # DELETE /appointments/1.json
  def destroy

      if (@appointment.date-Time.zone.now) > 8.hours
        @appointment.destroy
        respond_to do |format|
        format.html { redirect_to appointments_url, notice: 'Appointment was successfully canceled.' }
        format.json { head :no_content }
        end
      else
        @appointment.errors[:date] << 'cannot cancel within 8 hours from appointment time.'
        respond_to do |format|
        format.html { render :edit}
        format.json { render json: @appointment.errors, status: :unprocessable_entity }
        end
      end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appointment
      @appointment = Appointment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appointment_params
      params.require(:appointment).permit(:date, :patientConcern, :physicianNotes, :patientId, :physicianId, :diagnosticCodeId)
    end
end


# GET appointment-hours/
def fetchAvailableHours(date, physicianId, currentAppointment)
   @availableHours = Array.new
   if !date.nil?; date=date.to_date;
   if !physicianId.nil?; physicianId=physicianId.to_i;
   sHour=date+8.hours
   eHour=date+16.5.hours
   i=0
   while sHour <= eHour
     if Time.zone.now < sHour  #checks if the appointment hour has passed.
     if !Appointment.where(:physicianId => physicianId , :date => sHour).any?
       @availableHours.insert(i,sHour)
       i+=1
     end
     end
     sHour+=30.minutes
   end
   if !currentAppointment.nil?; @availableHours.insert(0,currentAppointment);end end end
   @availableHours
end

def getHours
end

def convertToHoursMinutes
  strftime('%I:%M %p')
end

def convertToddMMyy
  strftime('%D')
end


